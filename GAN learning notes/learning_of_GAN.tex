\documentclass[12pt,a4paper,final]{article}


\usepackage[left=4cm,right=2.5cm,top=2cm,bottom=2cm]{geometry}
\usepackage{times}
\usepackage{dirtree,float} % simple tree
\usepackage{caption}
\usepackage[bottom]{footmisc} %force display footnotes at page end after figures at page bottom
\renewcommand\footnotelayout{\fontsize{8.5}{0}\selectfont} %change footnotes @ page end-size


\usepackage{balance} % to better equalize the last page
\usepackage{graphics} % for EPS, load graphicx instead 
\usepackage[T1]{fontenc}
%\usepackage{txfonts}
\usepackage{mathptmx}
\usepackage{amsmath}
%\usepackage[pdftex]{hyperref}
\usepackage[colorlinks,linkcolor=black,anchorcolor=black,citecolor=green]{hyperref}
\usepackage{color}
\usepackage{booktabs}
\usepackage{textcomp}
\usepackage{microtype} % Improved Tracking and Kerning
\usepackage{ccicons} % Cite your images correctly!
%\usepackage[margin=1in]{geometry}
\usepackage{subfig}
\usepackage{graphicx}

\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{todonotes}
\usepackage{algorithm}
\usepackage{algpseudocode}

\begin{document}

\tableofcontents
\newpage
The generative models have become very popular in the CS field recently as they have achieved tremendous success in recent years.
By learning the true data distribution of the training set, the generative models can generate new data points which resemble the real ones after training. Among these models, the most commonly used generative approaches are Generative Adversarial Networks (GAN) and Variational Autoencoders (VAE).


\section{Vanilla GAN}
\subsection{The structure and algorithm of basic GAN}
\begin{figure}[!h]
\centering
  \includegraphics[width=0.5\linewidth]{figs/gan_strucutre}
  \caption{The structure of basic GAN}
%Source: https://aws.amazon.com/cn/blogs/machine-learning/combining-deep-learning-networks-gan-and-siamese-to-generate-high-quality-life-like-images/
\end{figure}
As the Figure 1 shows, the basic/vanilla GAN network is mainly comprised of a generator($G$) and a discriminator($D$). The function of the generator is to generate data points based on the input $z$, which is a random variable. The function of the discriminator is to discriminator whether the input data point is real or faked. Here we present the algorithm in pseudocode.

\begin{algorithm}
\caption{GAN algorithm}\label{euclid}
\begin{algorithmic}[1]
\For{\texttt{number of training iterations}}
	\For{\texttt{k steps}}
		\State Get $m$ generated images
		\State Get $m$ real images
		\State Update/train the discriminator with the real and faked images
	\EndFor
	\State Get $m$ generated images
	\State Update/train the generator based on the gradient provided by the discriminator
\EndFor
\end{algorithmic}
\end{algorithm}
\subsection{Objective function}
\textbf{Notation list}\\
$P_{data}$: The distribution of real image \\
$P_{z}$: The distribution of the random variable $z$\\
$x$: A real image sample\\
$G(z)$: Can be realized as the random image sample from $P_G$, where $P_G$ is the distribution of generated images\\
$D(x)$ and $D(G(z))$: The evaluated probability by $D$ that image $x$, $G(z)$ is real\\
\\
\textbf{Objective function Expression1}
\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  E_{x\sim P_{data}}[log(D(x))]+E_{z\sim P_{z}}[log(1- D(G(z)))]
\end{equation}
\textbf{Objective function Expression2}
\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  E_{x\sim P_{data}}[log(D(x))]+E_{x\sim P_G}[log(1- D(x))]
\end{equation}

Where $P_G$ is used to replace $G(z)$.\\
\\
\textbf{Objective function Expression3}
\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  \int_{P_{data}}P_{data}(x)log(D(x))dx+\int_{P_G}P_G(x)log(1- D(x))dx
\end{equation}
\textbf{Objective function Expression4}
\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  \int_{x}[P_{data}(x)log(D(x))+P_G(x)log(1- D(x))]dx
\end{equation}

The intuitive of the objective function can be quit simple. The $log(D(x))$ and $log(1-D(x))$ represent the log probability of correct discrimination. So the $D$, is trained to max the objective function, as it tries to correctly identify whetherthe image is faked. Contrarily, the $G$ is trained to max the $E_{x\sim P_G}[log(1- D(x))]$ part.

\subsection{Origin of the obejective function}
In machine learning field, the \textbf{cross entropy} is commonly used to represent the loss values. 
\begin{equation}
H(p,q)= H(p)+D_{KL}(p||q) = -\sum p\ log(q) 
\end{equation}
Where $ H(p)$ is the entropy of p: 
$$H(p)= -\sum p\ log(p)$$ 
$D_{KL}(p||q)$ is the Kullback-Leibler (KL) divergence between $p$ and $q$
$$D_{KL}(p||q): -\sum p\ log(\frac{q}{p})$$\\
When given a real or faked image $x_i$ with  its label $y_i$, the evaluation result (in probability) given by discriminator is $D(x_i)$. The loss function of discriminator can be expressed as:

$$
H(y_i,D(x_i)) =  - y_i \ log(D(x_i))- (1-y_i) \ log(1-D(x_i))
$$
So for $N$ given images, the average loss will be:
\begin{equation}
\frac{H_{total} }{N}=  \frac{-\sum_i^Ny_i \ log(D(x_i))-\sum_i^N(1-y_i) \ log(1-D(x_i))}{N}
\end{equation}
$$
=\frac{-\sum_i^Ny_i \ log(D(x_i))}{N}-\frac{\sum_i^N(1-y_i) \ log(1-D(x_i))}{N}
$$
As $y_i=0$ when image is faked and $1-y_i=0$ when image is real, the above equation can tranform to:
\begin{equation}
\frac{-\sum\limits_{i\in R}1*log(D(x_i))}{N}-\frac{\sum\limits_{i\in F}1*log(1-D(x_i))}{N}
\end{equation}
As we will take \textbf{the sames number} (i.e. both $N/2$) real and faked images for the training, the above equation can tranform to
$$
\frac{-N/2*E_{i\in R}[log(D(x_i))]}{N}-\frac{-N/2*E_{i\in F}[log(1-D(x_i))]}{N}
$$
\begin{equation}
= - \frac{1}{2}E_{x\sim real} [ log(D(x)]-
\frac{1}{2}E_{x\sim faked} [log(1-D(x))]
\end{equation}
So the objective function can be expressed as
\begin{equation}
\underset{D}{MAX}\ \ \frac{1}{2}E_{x\sim real} [ log(D(x)]+
\frac{1}{2}E_{x\sim faked} [log(1-D(x))]
\end{equation}
During the phase of generator training, since only half of the equation involved $G$, so the objective function now becomes:
\begin{equation}
\underset{}{MIN}\ \frac{1}{2}E_{x\sim faked} [log(1-D(x))]=
\underset{G}{MIN}\ \frac{1}{2}E_{z\sim P_Z} [log(1-D(G(z)))]
\end{equation}
Which can be expressed as
$$
\underset{G}{MAX}\ \frac{1}{2}E_{z\sim P_Z} [log(D(G(z)))]
$$
By combining the objective functions from the discriminator training phase and generator training phase, the final objective function can be expressed as:
\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  E_{x\sim real}[log(D(x))]+E_{z\sim P_{z}}[log(1- D(G(z)))]
\end{equation}



\subsection{Global optimality}
\textbf{When $G$ is fixed}\\
According to the 4th objective function expression, when $G$ is fixed, the optimal $D^*_G(x)$ is:  
\begin{equation}
D^*_G(x)= \frac{P_{data}(x)}{P_{data}(x)+P_G(x)}
\end{equation}
Proof. \\
Take $P_{data}(x)$ as $a$, $P_G(x)$ as $b$ and $D(x)$ as $y$, then:
\begin{equation}
\frac{d[a*log(y))+b*log(1-y)]}{dy} = 0\ \ \ when\ \ y=\frac{a}{a+b}
\end{equation}
\textbf{When $D$ is fixed}\\
When take the $D^*_G(x)$ in to the 3rd objective expression, we get:
\begin{equation}
\underset{G}{MIN}\ \  \int_{P_{data}}P_{data}(x)log(\frac{P_{data}(x)}{P_{data}(x)+P_G(x)})dx+\int_{P_G}P_G(x)log(\frac{P_{G}(x)}{P_{data}(x)+P_G(x)})dx
\end{equation}
It achieves its minimum at $P_{data}(x)=P_G(x)$ {\color{red}}. Following is the proof.\\


We can express the objetive function as:

\begin{equation}
-log(4)+\int_{P_{data}}log(2)P_{data}(x)dx+\int_{P_G}log(2)P_{G}(x)dx
\end{equation}
$$
+\int_{P_{data}}P_{data}(x)log(\frac{P_{data}(x)}{P_{data}(x)+P_G(x)})dx+\int_{P_G}P_G(x)log(\frac{P_{G}(x)}{P_{data}(x)+P_G(x)})dx
$$
\begin{equation}
=-log(4)
+\int_{P_{data}}P_{data}(x)log(\frac{2P_{data}(x)}{P_{data}(x)+P_G(x)})dx+\int_{P_G}P_G(x)log(\frac{2P_{G}(x)}{P_{data}(x)+P_G(x)})dx
\end{equation}
\begin{equation}
=-log(4)
+KL(P_{data}\vert\vert\frac{P_{data}(x)+P_G(x)}{2})+KL(P_{G}\vert\vert\frac{P_{data}(x)+P_G(x)}{2})
\end{equation}
\begin{equation}
=-log(4)
+2*JSD(P_{data}\vert\vert P_{G})
\end{equation}
Since the Jensen-shannon divergence $JSD(P_{data}\vert\vert P_{G}) $ is always non-negative, and zero if and only if $P_{data}$ and $P_{G}$ are equal. So when the generative model can perfectly replicating the data distribution, the global minimum is achieved at $-log(4)$.







\section{Conditional GAN}
\begin{figure}[!h]
\centering
  \includegraphics[width=0.5\linewidth]{figs/the_data_flow_of_cgan}
  \caption{The data flow of CGAN. }
%Source: https://medium.com/@jonathan_hui/gan-cgan-infogan-using-labels-to-improve-gan-8ba4de5f9c3d
\end{figure}
The conditional GAN (CGAN) must be the most import variant of original GAN nowadays.
It uses extra label information to generate images with better qualities and being able to control. While in the original GAN, there is no control over modes of the data to be generated. CGAN changes that by adding the label y as an additional parameter to the generator and hopes that the corresponding images are generated.
Figure1 shows the structure of the algorithm.
As it shows, labels act as an extension to the latent space $z$ to generate and discriminate images better. Although the structure of CGAN is simple, the whole mechanism why it works is still not fully understood. The cost function for CGAN is almost the same as GAN.

\begin{equation}
\underset{G}{MIN}\ \ \underset{D}{MAX}\ \  E_{x\sim P_{data}}[log(D(x|y))]+E_{z\sim P_{z}}[log(1- D(G(z|y)))]
\end{equation}



\section{Cycle consistency GAN}
\begin{figure}[h!]
	\centering
  \includegraphics[width=1\linewidth]{figs/ccgan_example}
  \caption{Example results from Cycle GAN. }
\end{figure}

After its birth in 2016, the Cycle consistency GAN (CC-GAN) has already attracted many researchers' attention for its great performance on image-to-image tasks.  The Figure 3 shows some example results generated by the CC-GAN. When taking an image from domain $X$ (e.g. a male photo), the CC-GAN can transform it into an image in another domain $Y$ (i.e. a female photo). As the Figure 4 shows, the CC-GAN is comprised of two generators ($G$,$F$) and two discriminators ($D_X$,$D_Y$).

\begin{figure}[h!]
	\centering
  \includegraphics[width=0.5\linewidth]{figs/cyclegan}
  \caption{The structure of Cycle GAN. }
	
%Source: https://www.microsoft.com/developerblog/2017/06/12/learning-image-image-translation-cyclegans/
\end{figure}
\noindent\textbf{Notation}\\
$G$: The generator which trasfer image from domain X to Y\\
$F$: The generator which trasfer image from domain Y to X\\
$D_X$: The descriminator for domain X\\
$D_Y$: The descriminator for domain Y
\subsection{Loss function}
The loss function of CC-GAN is made up of three parts: 1. the first GAN loss 2. the second GAN loss 3. The cycle consistency loss. As the CC-GAN is applied for image-to-image translation, a image from the other domain will be applied as input to replace the latent vector $z$ in the original GAN equation. Use the equation 1 as the base, the first GAN loss is:
\begin{equation}
\mathcal{L}_{GAN1}=E_{y\sim P_{y\_data}}[log(D_Y(y))]+E_{x\sim P_{x\_data}}[log(1- D_Y(G(x)))]
\end{equation}
The second GAN loss is:
\begin{equation}
\mathcal{L}_{GAN2}=E_{x\sim P_{x\_data}}[log(D_X(x))]+E_{y\sim P_{y\_data}}[log(1- D_X(F(y)))]
\end{equation}
The cycle consistency loss:
\begin{equation}
\mathcal{L}_{cyc}=E_{x\sim P_{x\_data}}[\ ||F(G(x))-x||_1]+E_{y\sim P_{y\_data}}[\ ||G(F(y))-y||_1]
\end{equation}
The total loss function is:
\begin{equation}
\mathcal{L}_{Total}= \mathcal{L}_{GAN1}+\mathcal{L}_{GAN2}+\mathcal{L}_{cyc}
\end{equation}
The objective function of CC-GAN is: 
\begin{equation}
\underset{G,F}{MIN}\ \ \underset{D_X,D_Y}{MAX}\ \  \ \mathcal{L}_{Total}
\end{equation}

\section{Variational autoencoder(VAE)}
The variational autoencoder(VAE) approach is the other popular method to generate data points. As the Figure 5 shows, it has the structure which very similar to autoencoder, which is comprised of an encoder, a latent variable and a decoder. However, it is very different when seeing from the mathematically perspective.\\
\begin{figure}[!h]
\centering
  \includegraphics[width=0.7\linewidth]{figs/VAE}
  \caption{The structure of VAE. }
\end{figure}

\noindent{\textbf{Notation}}\\
$z$: The latent variable, it decide the main contont of the generated image.\\
$X$: Can be recognized as a training sample in the data.

\subsection{Key ideas}
\begin{enumerate}
\item The original idea of VAE is similar to GAN. Given the image sample set $\{ X_1,X_2,...\}$, we want to learn a generator which has the distribution which can max the probability of getting the samples.
\item To apply a generative model, the consideration of latent variables is necessary. For example, if we want to generate a digit, then we should tell the model which number to generate first. VAEs assume there is no simple interpretation of the dimensions of $z$, but samples of z can be drawn from simple distributions like $\mathcal{N}(0,I)$. This is because any distribution in d dimensions can be generated by taking a set of d variables that are normally distributed and mapping them through a sufficiently complicated function. This is an extension of "inverse transform sampling", as in one dimension, we can generate any distribution use the inverse cumulative distribution function. The direct learning of target distribution can be very complicated, the applying of an indirect method, apply the latent variable $z$ helps a lot.

\begin{equation}
z\sim \mathcal{N}(0,I)
\end{equation}
\begin{figure}[!h]
\centering
  \includegraphics[width=0.5\linewidth]{figs/z_to_images}
  \caption{Generation based on latent variable }
\end{figure}

\item The reconstructed image $\hat{x}$, can have the distribution of $\hat{x} \sim \mathcal{N} (f(z),I)$. Where function $f$ is the generator.
\item We can think the probability of getting a real image $x$ from our generator is $P(x)$.
\begin{equation}
P(x)=\int P(x|z;\theta)P(z)dz=\int \mathcal{N} (x|f(z),I) P(z)dz
\end{equation}
And our aim is the max the $P(x)$ in training. In real computation, $P(x)\approx \frac{1}{n}\sum^n_{i=0}P(x|z_i)$. It looks easy, however, one problem emerges: most $z_i$ will lead to $P(x|z_i)\approx 0 $.

\item To solve this problem, authors try to find a distribution $q(z)$ or $q(z|x)$, which can approximate to $P(z|x)$. From which we can sample $z$ that $P(x|z)>>0$.
 
\item So now our new aim become to max $P(x)$ while min $KL(q(z|x)||p(z|x))$:
\begin{equation}
MAX\ \  log(p(x))- KL(q(z|x)||p(z|x))
\end{equation}
Interestly, we found:
\begin{equation}
log(p(x))- KL(q(z|x)||p(z|x))= \sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)})
\end{equation}
As:
$$
KL(q(z|x)||p(z|x)) = -\sum_Zq(z|x)log(\frac{p(z|x)}{q(z|x)})
$$
$$
= -\sum_Zq(z|x)log(\frac{p(z,x)}{p(X)q(z|x)})=-\sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)})+\sum_Zq(z|x)log(p(x))
$$
\begin{equation}
=-\sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)})+log(p(x))\sum_Zq(z|x)=-\sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)})+log(p(x))
\end{equation}

Hence:
\begin{equation}
MAX\ \  log(p(x))- KL(q(z|x)||p(z|x))= MAX\ \ \sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)})
\end{equation}

\item 
As 
$$
\sum_Zq(z|x)log(\frac{p(z,x)}{q(z|x)}) = 
\sum_Zq(z|x)log(\frac{p(x|z)p(z)}{q(z|x)})
$$
$$
=\sum_Zq(z|x)log(p(x|z))+\sum_Zq(z|x)log(\frac{p(z)}{q(z|x)})
$$
$$
=E_{z\sim q(z|x)}log(p(x|z))-KL(q(z|x)||p(z))
$$
We trsnfer the objetive function into:
\begin{equation}
MAX\ \ E_{z\sim q(z|x)}log(p(x|z))-KL(q(z|x)||p(z))
\end{equation}
\item 
$E_{z\sim q(z|x)}log(p(x|z))$ can be recognized as the reconstruction error.  As $\hat{x} \sim \mathcal{N} (f(z),I)$, $E_{z\sim q(z|x)}log(p(x|z))$ is equal to $E_{z\sim q(z|x)}log(\mathcal{N}(x|f(z),I)$. Max $E_{z\sim q(z|x)}log(\mathcal{N}(x|f(z),I)$ is equal to min $E_{z\sim q(z|x)}\vert \vert f(z)-x\vert \vert^2$
\item $KL(q(z|X)||p(z))$ can be think as a constrain part as we make assumption that $z$ follows a $\mathcal{N}(0,I)$ distribution.
\end{enumerate}

\section{UNIT network}
\begin{figure}[h!]
  \includegraphics[width=1\linewidth]{figs/cat_translation}
  \caption{Examples results of UNIT network}
\end{figure}
The unsupervised image-to-image translation (UNIT) network is a new algorithm invented in 2017. It achieves outstanding results on specific image-to-image translation tasks. As the Figure 7 shows, the UNIT newwork is very good at translating the images from similar domains.

The authors of the UNIT network make a strong assumption about the shared-latent space. They assume for any given pair images $x_1$ and $x_2$, there exists a shared latent code $z$ in a shared-latent space. Such people can recover both images from this $z$, and compute this $z$ from each of the two
images.

Specifically, they postulate there exist encoder $E_1$ , $E_2$ and generator $G_1$, $G_2$ such that, given a pair of
corresponding images ($x_1$, $x_2$) from the joint distribution, it can have $z = E_1 (x_1) = E_2 (x_2)$ and conversely $x_1 = G_1 (z)$ and $x_2 = G_2(z)$.

\begin{figure}[h]
  \includegraphics[width=1\linewidth]{UNIT_structure}
  \caption{The strucutre of UNIT GAN.}
  \label{fig:The strucutre of UNIT GAN}
\end{figure}
\noindent{\textbf{The loss function of UNIT network is made up by six parts.}}\\

\noindent{\textbf{VAE Loss}}
\begin{equation}
\mathcal{L}_{VAE_1}=KL(q_a(z_a|x_a)||P(z))
-E_{z_a\sim q_a(z_a|x_a)}[log\ P_{G_1}(x_a|z_a)]
\end{equation}
Where $G_1$ is the combination of $G_S$ and $G_A$
\begin{equation}
\mathcal{L}_{VAE_2}=KL(q_b(z_b|x_b)||P(z))
-E_{z_b\sim q_b(z_b|x_b)}[log\ P_{G_2}(x_b|z_b)]
\end{equation}

\noindent{$G_2$ is the combination of $G_S$ and $G_A$}\\

\noindent{\textbf{GAN Loss}}\\
\begin{equation}
\mathcal{L}_{GAN_1}=E_{x_a\sim P_{x_a\_data}}[log(D_A(x_a))]
+E_{z_b\sim q_b(z_b|x_b)}[log(1- D_A(G_1(z_b)))]
\end{equation}

\begin{equation}
\mathcal{L}_{GAN_2}=E_{x_b\sim P_{x_b\_data}}[log(D_B(x_b))]
+E_{z_a\sim q_a(z_a|x_a)}[log(1- D_B(G_2(z_a)))]
\end{equation}

\noindent{\textbf{Cycle consistency Loss}}\\
\begin{equation}
\mathcal{L}_{CC_1}=KL(q_a(z_a|x_a)||P(z))+KL(q_b(z_b|x_a^{a\rightarrow b})||P(z))-
E_{z_b\sim q_b(z_b|x_a^{a\rightarrow b})}[log\ P_{G_1}(x_a|z_b)]
\end{equation}
\begin{equation}
\mathcal{L}_{CC_2}=KL(q_b(z_b|x_b)||P(z))+KL(q_a(z_a|x_b^{b\rightarrow a})||P(z))-
E_{z_a\sim q_a(z_a|x_b^{b\rightarrow a})}[log\ P_{G_2}(x_b|z_a)]
\end{equation}
\noindent{\textbf{Total Loss}}\\
\noindent{By combining all the above loss functions, it now becomes the total loss function}
\begin{equation}
\mathcal{L}_{Total}= \mathcal{L}_{VAE_1}+\mathcal{L}_{VAE_2}+\mathcal{L}_{GAN_1}+\mathcal{L}_{GAN_2}+\mathcal{L}_{CC_1}+\mathcal{L}_{CC_2}
\end{equation}
\noindent{\textbf{Objective function}}
\begin{equation}
\underset{E_1,E_2,G_1,G_2}{MIN}\ \ \underset{D_A,D_B}{MAX}\ \  \ \mathcal{L}_{Total}
\end{equation}
Where $E_1$ is the combination of $E_S$ and $E_A$, and $E_2$ is the combination of $E_S$ and $E_B$.


\section{Others}
\subsection{Deep convolutional GAN(DCGAN)}
Use the same objective function of the Vanilla GAN, the main difference is to use the covolutional networks and decovolutional networks in place of MLP. 


\subsection{The problem of original loss function and the Wasserstin GAN}
As we see in the first section, the Goodfellow actually apply the cross entropy to measure the distance between two probability distributions. Since the distance function contains the 'log' part(as Divergence-KL makes a part of cross entropy). It cause problem of "extrem large/small values" when $D(x)$ returen a value near 1 or near zero. 

\begin{figure}[h]
  \includegraphics[width=1\linewidth]{prob_dist_example}
  \caption{The distance between two probabilities}
  \label{fig:The distance between two probabilities}
\end{figure}
As illustrates by the a figure, by using the popular distance measures like KL-divergence, JS-divergence, even when the target distribution beomcing similar, the 'distance' values might not able to reveal these. So according to the researchers like Martin, they suggest to use the Wasserstein distance instead(however, computing of Wasserstein distance is not easy too xD ). And in the Wasserstin GAN paper, the authors proposed a approximate algorithm which using the following equation( which I have not understood...)
\begin{figure}[h]
	\centering
  \includegraphics[width=0.5\linewidth]{approxiation_wass}
\end{figure}

\subsubsection{Implementation hints}
Dispite the theoretical part of wasserstin paper looks really complex, its change of implementation to the original GAN is quit easy:\\
1. No log in the loss function \\
2. The output of D is no longer to be a probability(no sigmoid in $D$)\\
3. Clip the weight of  D \\
4. Train  D  more than  G\\
5. Use RMSProp instead of ADAM\\
6. Using lower learning rate

\subsection{Maximum mean disc(MMD)}
Say for n samples $x$ of $P$ and m samples of $y$ from $Q$, there exists some given function $\phi()$

$$\mu_{\phi(x)} = \frac{1}{n}\sum \phi(x_i); \ \ \ \ \mu_{\phi(y)} = \frac{1}{m}\sum \phi(y_i)$$ 
MMD states that the distance between two distribution can be measure by the distance $|\mu_{\phi(y)} - \mu_{\phi(x)}|$

$$
||\mu_{\phi(y)} - \mu_{\phi(x)}||^2 = (\frac{1}{n}\sum^n_{i=1}\phi(x_i)-\frac{1}{m}\sum^m_{j=1}\phi(y_j))^T(\frac{1}{n}\sum^n_{i=1}\phi(x_i)-\frac{1}{m}\sum^m_{j=1}\phi(y_j))
$$
$$
=\frac{1}{n^2}\sum^N\sum^N\phi(x_i)^T\phi(x_j)+\frac{1}{m^2}\sum^M\sum^M\phi(y_i)^T\phi(y_j)+\frac{2}{mn}\sum^M\sum^N\phi(x_i)^T\phi(y_j)
$$
We can replace $\phi(x_i)^T\phi(x_j)$, $\phi(y_i)^T\phi(y_j)$ and $phi(x_i)^T\phi(y_j)$ with kerels.
\subsection{Notation}
$x_a$: The image from domain A \\
$x_b$: The image from domain B \\
$z$: The lantent space vector \\

\noindent$E_A$: The encoder for domain A \\
$E_B$: The encoder for domain B \\
$E_S$: The shared encoder for domain A and B \\

\noindent$G_S$: The shared decoder for domain A and B \\
$G_A$: The decoder for domain A\\
$G_B$: The decoder for domain B\\

\noindent$D_A$: The descriminator for domain A\\
$D_B$: The descriminator for domain B\\

\subsection{Equations}
$$z_a = E_S(E_A(x_a))+\mathcal{N}(0,1)$$
$$z_b = E_S(E_B(x_b))+\mathcal{N}(0,1)$$
$$x_{aa}=G_A(G_S(z_a))$$
$$x_{ab}=G_B(G_S(z_a))$$
$$x_{ba}=G_A(G_S(z_b))$$
$$x_{bb}=G_B(G_S(z_b))$$
$$z_{aa} = E_S(E_A(x_{aa}))+\mathcal{N}(0,1)$$
$$z_{ab} = E_S(E_B(x_{ab}))+\mathcal{N}(0,1)$$
$$z_{ba} = E_S(E_A(x_{ba}))+\mathcal{N}(0,1)$$
$$z_{bb} = E_S(E_B(x_{bb}))+\mathcal{N}(0,1)$$

$$x_{aba}=G_A(G_S(z_{ab}))$$

$$
Loss_{G_{total}} = Loss_{G_{A}}+Loss_{G_{B}}
$$
$$
Loss_{G_{A}} = \lambda_1*MSE(I-D_A(x_{ba}))+\lambda_2*MAE(x_a-x_{aa})
$$
$$
+\lambda_3*MAE(x_a-x_{aba})+\lambda_4*MSE(z_a,z_b)+\lambda_5*MSE(z_{ba})
$$
$$
Loss_{G_{B}} = \lambda_1*MSE(I-D_B(x_{ab}))+\lambda_2*MAE(x_b-x_{bb})
$$
$$
+\lambda_3*MAE(x_b-x_{bab})+\lambda_4*MSE(z_a,z_b)+\lambda_5*MSE(z_{ab})
$$
  

$$
Loss_{D_{total}}=Loss_{D_A}+Loss_{D_B}
$$
$$
=\lambda6(MSE(D_A(x_a)-I)+MSE(D_A(x_{ba})))+\lambda7(MSE(D_B(x_b)-I)+MSE(D_B(x_{ab})))
$$



\end{document}
