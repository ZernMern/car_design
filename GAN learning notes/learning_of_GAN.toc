\contentsline {section}{\numberline {1}Vanilla GAN}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}The structure and algorithm of basic GAN}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Objective function}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Origin of the obejective function}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Global optimality}{4}{subsection.1.4}
\contentsline {section}{\numberline {2}Conditional GAN}{5}{section.2}
\contentsline {section}{\numberline {3}Cycle consistency GAN}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Loss function}{7}{subsection.3.1}
\contentsline {section}{\numberline {4}Variational autoencoder(VAE)}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Key ideas}{8}{subsection.4.1}
\contentsline {section}{\numberline {5}UNIT network}{9}{section.5}
\contentsline {section}{\numberline {6}Others}{11}{section.6}
\contentsline {subsection}{\numberline {6.1}Deep convolutional GAN(DCGAN)}{11}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}The problem of original loss function and the Wasserstin GAN}{11}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Implementation hints}{12}{subsubsection.6.2.1}
\contentsline {subsection}{\numberline {6.3}Maximum mean disc(MMD)}{12}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Notation}{12}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Equations}{13}{subsection.6.5}
